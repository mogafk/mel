import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'

import Comment from './Comment';
import CommentForm from './CommentForm';
import { addComment } from './actions'


var commentsData = require('./comments.json')["comments"];

class CommentList extends Component{

  render() {
    return (
      <div>
        Количество комментариев: {this.props.comments.length}
        {this.props.comments.map((commentData) => <Comment key={commentData['id']} 
										        	likes={commentData['likes']} 
										        	text={commentData['text']}
										        	answers={commentData['replies']}
										        	canReply={true}/>)}

        <div>
          <CommentForm addComment={this.props.addComment}/>
        </div>
      </div>
    );
  }
}

const mapStateToProps = function(store) {
  console.log('store', store)
  return {
    comments: store.comments
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addComment: bindActionCreators(addComment, dispatch)
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(CommentList);
// export default connect(mapStateToProps)(CommentList);
// export default CommentList