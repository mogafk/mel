import React, {Component} from 'react'

import Comment from './Comment'
import CommentList from './CommentList'


class App extends Component{
  render() {
    return (
      <div>
        <CommentList />
      </div>
    );
  }
}

export default App