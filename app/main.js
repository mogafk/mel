import React from 'react';
import {render } from 'react-dom';
import { Provider } from 'react-redux'
import { createStore } from 'redux'


import App from './App';
import commentsApp from './reducers';
// console.log('commentsApp', commentsApp)
let store = createStore(commentsApp)


// render(<App />, document.getElementById('root'));
render(
	<Provider store={store}>
		<App />
	</Provider>,

	document.getElementById('root')
	);