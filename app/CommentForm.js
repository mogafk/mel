import React, {Component} from 'react'

class CommentForm extends Component{
  constructor(props){
  	super(props);

  	this.addComment = this.addComment.bind(this);
  }

  addComment() {
  	// console.log(this.refs.commentText.value)
  	// console.log(this.props.addComment)

  	let text = this.refs.commentText.value;
  	this.props.addComment(text, 'user value');
  }
  render() {
    return (
      <div>
      	<div>
      	  Добавить комментарий
      	</div>
        <textarea ref='commentText'></textarea>
        <div>
          <button onClick={this.addComment}>Отправить</button>
        </div>
      </div>
    );
  }
}

// function mapStateToProps(state) {
//   return {
//     // user: state.user,
//     // page: state.page
//   }
// }

// function mapDispatchToProps(dispatch) {
//   return {
//     // pageActions: bindActionCreators(pageActions, dispatch)
//   }
// }


export default CommentForm