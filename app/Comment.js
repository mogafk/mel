import React, {Component} from 'react'
import './comment.css';

class Comment extends Component{
  constructor(props){
  	super(props);
    this.addLike = this.addLike.bind(this);
  }

  addLike(){
  	//ВОТ тут о чем я говорил
  	console.log('addLike');
  	this.setState({
  		likes: this.props.likes+1
  	})
  	console.log(this.state.likes)
  	console.log(this.props.likes)
  }

  render() {
  	var replies = '';
  	if(this.props.answers){
  		replies = this.props.answers.map((reply) => <Comment 
  			key ={reply['id']}
  			text={reply['text']}
  			likes={reply['likes']}
  			canReply={false}/>
  			)
  	}

    return (
      <div className="comment">
      	<div>
      		Количество лайков: {this.props.likes} <span onClick={this.addLike}> +1 </span>
      	</div>
        {this.props.text}

        <div className={this.props.canReply ? "comment_reply" : "comment_reply comment_reply--hide"}>
          Ответить
        </div>

        <div className="comment_replies">
        	{replies}
        </div>
      </div>
    );
  }
}

export default Comment