
const comment = (state = {}, action) => {
  switch (action.type) {
    case 'ADD_COMMENT':
      return {
        id: action.id,
        text: action.text,
        completed: false
      }

    default:
      return state
  }
}

// const comments = (state = [], action) => {
// const comments = (state = require('../comments.json'), action) => {
const comments = (state = require('../comments.json')['comments'], action) => {
  switch (action.type) {
    case 'ADD_COMMENT':
      return state.concat({
        text: action.text,
        user: action.user,
        likes: action.likes,
        id: action.id,
      })
      // return [
      //   ...state,
      //   comment(undefined, action)
      // ]
    default:
      return state
  }
}


export default comments