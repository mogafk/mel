let nextTodoId = 999

export const addComment = (text, user) => {
  return {
    type: 'ADD_COMMENT',
    id: nextTodoId++,
    likes: 0,
    text, user
  }
}